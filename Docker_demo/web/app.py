import os
import json
import jwt
from flask import Flask, redirect, url_for, request, render_template,jsonify, make_response
from pymongo import MongoClient
from flask_cors import CORS,cross_origin
from flask_mongoengine import MongoEngine
import mongoengine as me
from flask_marshmallow import Marshmallow
app = Flask(__name__)
from flask_jwt_extended import JWTManager
from datetime import datetime, timedelta
from functools import wraps
# client = MongoClient("mongodb://db:27017")
# db = client['tododb']

CORS(app)
ma= Marshmallow(app)
app.config['MONGODB_SETTINGS'] = {
    'host': 'mongodb://db:27017'
}
db = MongoEngine(app)

class Users(db.Document):
    Id = me.IntField(default=1)
    firstname = me.StringField(required=True)
    lastname = me.StringField(required=True)

@app.route('/')
def todo():
    all_link = Users.objects()
    hh = short_schema.dump(all_link)

    # _items = db.tododb.find()
    # items = [item for item in _items]
    # hh = json.dumps(items, default=str)
    return jsonify(hh)

class UsersSchema(ma.Schema):
    class Meta:
        fields = ('Id', 'firstname', 'lastname')

short_schema = UsersSchema(many = True)

@app.route('/new', methods=['Post'])
@cross_origin()
def new():

    # item_doc = {
    #     'name': 'laxman',
    #     'description': 'dockerwala'
    # }
    # db.tododb.insert_one(item_doc)
    
    Users(Id=request.json['Id'], firstname= request.json['firstname'], lastname= request.json['lastname']).save()
    return "inserted"






JWTManager(app)


class User(db.Document):
    Id = me.IntField(default=1)
    firstname = me.StringField(required=True)
    lastname = me.StringField(required=True)
    contact = me.StringField(required=True)
    email = me.StringField(required=True)
    address = me.StringField(required=True)
    salary = me.StringField(required=True)
    role = me.StringField(required=True)

class UserSchema(ma.Schema):
    class Meta:
        fields = ('Id', 'firstname','lastname','contact','email','address','salary','role')

user_schema = UserSchema()
usersschema = UserSchema(many =True)

class Covid(db.Document):
    Id = me.IntField()
    country = me.StringField(required=True)
    total_Cases = me.IntField()
    active_cases = me.IntField()
    dead = me.IntField()

class CovidSchema(ma.Schema):
    class Meta:
        fields = ('total_Cases','country','active_cases','dead')

covid_schema = CovidSchema()
covidssschema = CovidSchema(many =True)


class ShortLink(db.Document):
    Id=me.IntField(default=1)
    original_link = me.StringField(required=True)
    visits = me.IntField(default=1)
    created_at = me.DateTimeField(default=datetime.utcnow)


class ShortLinkSchema(ma.Schema):
    class Meta:
        fields = ('_id', 'original_link', 'visits')

short_schema = ShortLinkSchema(many = True)


@app.route('/user/login', methods=['Post'])
@cross_origin()
def loginuser():
    auth = request.authorization
    if not auth or not auth.username or not auth.password:
        return make_response('could not verify', 401 ,{ 'www-authenticate': 'Basic realm= "Login required!"' })
    user = User.objects.get(email=auth.username).to_json()
    user = json.loads(user)
    if not user:
        status = 'please Enter valid Email'
        # return make_response('Please check Email!!', 401 ,{ 'www-authenticate': 'Basic realm= "Login required!"' })
    else:
        if user['contact'] == auth.password:
            token = jwt.encode({'email':user['email'],
            'firstname': user['firstname'],
            'lastname': user['lastname']},
            'secret')

            return jsonify({'token':token.decode('UTF-8'), 'status': 'ok'})

        else:
            status = 'Enter Correct Password'
            # return make_response('Please check password', 401 ,{ 'www-authenticate': 'Basic realm= "Login required!"' })
    return jsonify({ 'status': status })


def token_required(f):
    @wraps(f)
    def decorated(*args, **kwargs):
        token = None
        if request.headers:
            token = request.headers.get('Authorization')
            if not token:
                # return make_response('could not verify', 401 ,{ 'www-authenticate': 'Basic realm= "Login required!"' })
                return jsonify({'message': 'token is missing!!'})
            try:

                data = jwt.decode(token, verify=False)
                current_user = "hh" 
                # User.objects.get(email = data['email']).to_json()
            except:
                print('nof logged')
                return make_response('could not verify', 401 ,{ 'www-authenticate': 'Basic realm= "Login required!"' })
                # return jsonify({'message':'Token is missing'})

        return f(current_user, *args, **kwargs)
    return decorated

count=3
@app.route('/link', methods=['Post'])
@cross_origin()
def insert_link():
    global count
    count = count +1
    body = request.get_json()
    ShortLink(original_link=body['original_link'], Id = count ).save()

    return 'Ok'

@app.route('/<id>',methods=['Get'])
@cross_origin()
def get_shortlink(id):
    link = ShortLink.objects.get(Id= id).to_json()
    links = json.loads(link)
    ShortLink.objects.get(Id=id).update(visits=links['visits'] + 1)
    return redirect(links['original_link'])

@app.route('/urls',methods=['Get'])
@token_required
@cross_origin()
def get_shortlinks(current_user):
    all_link = ShortLink.objects()
    result = short_schema.dump(all_link)
    return jsonify(all_link)

    
@app.route('/link/<id>', methods=['DELETE'])
@token_required
@cross_origin()
def delete_link(current_user, id):
    ShortLink.objects.get(Id=id).delete()
    return '', 200

user_id =1
@app.route('/user',methods=['Post'])
@token_required
@cross_origin()
def create_user(current_user):
    global user_id
    user_id = user_id + 3
    newuser_obj = User()
    newuser_obj.Id = user_id
    newuser_obj.firstname = request.json['firstname']
    newuser_obj.lastname = request.json['lastname']
    newuser_obj.contact = request.json['contact']
    newuser_obj.email = request.json['email']
    newuser_obj.address = request.json['address']
    newuser_obj.salary = request.json['salary']
    newuser_obj.role = request.json['role']
    newuser_obj.save()
    return jsonify(ok=True)

@app.route('/users', methods=['GET'])
@token_required
@cross_origin()
def get_users(current_user):
    all_users = User.objects()
    result = usersschema.dump(all_users)
    return jsonify(result)

@app.route('/users/<id>', methods=['GET'])
@token_required
@cross_origin()
def get_user(current_user, id):
    user = User.query.get(id)
    return user_schema.jsonify(user)


@app.route('/users/<id>', methods=['PUT'])
@token_required
@cross_origin()
def update_user(current_user, id):
    body = request.get_json()
    User.objects.get(Id=id).update(**body)
    return '', 200

@app.route('/users/<id>', methods=['DELETE'])
@token_required
@cross_origin()
def delete_user(current_user, id):
    User.objects.get(Id=id).deleteMany()
    return '', 200

@app.route('/covid',methods=['GET'])
@token_required
@cross_origin()
def get_covid_cases(current_user):
    all_cases = Covid.objects()
    result = covidssschema.dump(all_cases)
    return jsonify(result)










if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)